# Generating a xUnit report with C/C++test Standard

To report unit tests results using xUnit format:

1. Copy the [`xunit.xsl`](xunit.xsl) file into a local directory (`<CPPTEST_XSL_DIR>`).
2. Copy [`Saxon`](https://gitlab.com/parasoft/cpptest-gitlab/-/blob/master/xsl/saxon) files into a local directory (`<CPPTEST_SAXON_DIR>`).
3. Update your GitLab pipeline to convert `<CPPTEST_REPORTS_DIR>/report.xml` report into `<CPPTEST_REPORTS_DIR>/report-xunit.xml`:

```yaml
    ...
    # Converts the unit tests report to xUnit format.
    #
    # To use Saxon for report transformation, a Java executable is required. 
    # C/C++test includes Java which can be used for this purpose.
    - echo "Generating xUnit report..."
    - $CPPTEST_INSTALL_DIR/bin/jre/bin/java -jar "$CPPTEST_SAXON_DIR/saxon-he-12.2.jar" -xsl:"$CPPTEST_XSL_DIR/xunit.xsl" -s:"$CPPTEST_REPORTS_DIR/report.xml" -o:"$CPPTEST_REPORTS_DIR/report-xunit.xml"
    
    artifacts:
      # Uploads test results in the xUnit format, so that they are displayed in GitLab
      reports:
        junit: $CPPTEST_REPORTS_DIR/report-xunit.xml
```
4. Run your GitLab pipeline.
